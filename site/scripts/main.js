/**
 * Main JavaScript
 * Site Name
 *
 * Copyright (c) 2015. by Way2CU, http://way2cu.com
 * Authors:
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

/**
 * Handle clicking on GetIn link.
 *
 * @param object
 */
Site.handle_link_click = function(event) {
	event.preventDefault();
	Site.frame_dialog.open();
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	// create dialog for payment
	var content = document.createElement('iframe');
	content.src = 'https://get-in.com/full-purchase/autobeer?fb=false&nav=false';
	content.style.width = Site.is_mobile() ? '90vw' : '600px';
	content.style.height = 'calc(100vh - 100px)';

	Site.frame_dialog = new Dialog({clear_on_close: false});
	Site.frame_dialog
		.add_class('purchase')
		.set_content(content);

	// attach click handler to links
	var links = document.querySelectorAll('a');
	for (var i=0, count=links.length; i<count; i++) {
		var link = links[i];
		if (link.href.indexOf('autobeer') > -1)
			link.addEventListener('click', Site.handle_link_click);
	}
};


// connect document `load` event with handler function
$(Site.on_load);
